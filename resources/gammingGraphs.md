# Exploring gaming data with graph databases

## Abstract
Here we showcase the entire workflow from fetching data from a public API
to presenting it in a nice graph. Some ETL (extract > transform > load) is
donne to make raw data in JSON format usable by neo4j.

## Introduction
[Giant Bomb] (http://www.giantbomb.com "Giant Bomb's home") is a site that
aggregates information about games, news, reviews, etc. Giant
Bomb makes most if its data available though a public API. We use this API
to retreive information about games consisting version, company, characters,
etc. After cleaning the data we load it to neo4j, a graph database engine.

Graphs are a great way to view data exposing relationshios among entities.
We had great fun playng with gaming data, exposing relatioships and digging
timelines and every other queries that came to our mind in a imediate and easy
interface. By the end of this post you will be able to generate expressive 
images like this, showing the deploymento of Fifa in several gaming platforms.
![Fifa platforms](fifa.graph.png)

## What is a graph database?
A graph database uses nodes and edges to create graph structures. Nodes are
entities with data, for instace nodes for Games with Name property and nodes
for Year. Edges stabish relations among node, for instace each year has several
games launches and a game may have more than one version. The figure bellow
shows what we can do with neo4j.

## Getting data
Giant Bomb has a public API, fairly easy to use, that will give us access to 
gaming data to play with. For information about [Giant Bomb]'s API go to 
(http://www.giantbomb.com/api/ "Giant Bomb's api"). To gain access to Giant 
Bomb's API you need to create an account. After getting credentials to gain
access to the API you will need to generate a token. After making your personal 
account you will be prompted to generate a token; save it in a secure place.

To do a simple query type in your favorite browser:
```html
http://www.giantbomb.com/api/search?api_key=[YOUR-KEY]&format=[RESPONSE-DATA-FORMAT]&query=[YOUR-SEARCH]&resources=[SOME-TYPES]
```
If you are looking for fifa soccer games (replace with your own key):
```html
http://www.giantbomb.com/api/search/?api_key=42eddc3161b1cf4165ca264665e02fd8159cee21&format=json&query=fifa&resources=game
```

## Cleaning data (ETL)


## Loading data to neo4j


## Exploring gaming data
The easyest way to explore data is playing with neo4j's interface. You can 
select a node or an edge and make it dispear, out constaints, etc.

## Conclusions
neo4j is a mature graph database engine that makes it easy to eplore
large data sets. It is fun to play with neo4j interactive user interface.
New relationships are constantly emerging in aesthetically apealing patterns.

The setup of this project was straight forward, highlighting how quik and
fun it is to fetch data from a public source and exploring it.
