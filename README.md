# README #

Article describing Goncalo Castro's neo4j project. In this project GiantBomb's 
public API is accessed to fetch games data. neo4j is used to present and explore
this data.

### What is this repository for? ###

* Display and explore data extracted from public APIs.
* Version 0.1

### Who do I talk to? ###

* Marionete
* goncalo.castro@marionete.co.uk
