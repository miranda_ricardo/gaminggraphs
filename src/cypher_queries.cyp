// Best selling video game franchises
// https://en.wikipedia.org/wiki/List_of_best-selling_video_game_franchises
WITH ["Tetris", "Super%20Mario", "Pokémon", "Call%20of%20Duty", "The%20Sims", "Need%20for%20Speed", "Final%20Fantasy", "Sonic", "Minecraft", "FIFA"] as games
UNWIND (games) as game
WITH 'http://www.giantbomb.com/api/search/?api_key=d578ee889ed9905ab6640cc0441153ade24afabe&format=json&query="gameTitle"&resources=game' AS tempURL, game
WITH replace(tempURL, "gameTitle",  game) as tempURL
CALL apoc.load.json(tempURL) YIELD value as jsonResponse
WITH range(0,toInt(jsonResponse.number_of_page_results) - 1) as nums, jsonResponse
UNWIND (nums) as num
WITH jsonResponse.results[num] AS game
WITH game, game.platforms AS platforms
UNWIND (platforms) as platform
MERGE (g:Game {name: game.name})
ON CREATE SET g.release_date = substring(game.original_release_date, 0, 4)
MERGE (p:Platform {platforms: platform.name})
MERGE (g)-[:FOR]->(p)